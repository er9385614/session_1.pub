import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:sessssion_1_2/common/colors.dart';
import 'package:sessssion_1_2/domain/otp_presenter.dart';
import 'package:sessssion_1_2/presentation/pages/new_password_page.dart';
import 'package:sessssion_1_2/presentation/pages/sign_up_page.dart';

import '../utils.dart';

class VerifyOTPPage extends StatefulWidget{

  final String email;

  const VerifyOTPPage({super.key, required this.email});
  @override
  State<VerifyOTPPage> createState() => _VerifyOTPPageState();
}

class _VerifyOTPPageState extends State<VerifyOTPPage> {
  var code = TextEditingController();

  var presenter = OTPPresenter();


  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83,),
            Text("Верификация",
              style: Theme.of(context).textTheme.titleLarge,),
            const SizedBox(height: 8,),
            Text("Введите 6-ти значный код из письма ",
              style: Theme.of(context).textTheme.titleMedium,),
            const SizedBox(height: 58,),
            Pinput(
              length: 6,
              controller: code,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              defaultPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1),
                  border: Border.all(
                    color: colors.subText
                  )
                )
              ),
              focusedPinTheme: PinTheme(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(1),
                      border: Border.all(
                          color: colors.subText
                      )
                  )
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressOTP(
                              code.text,
                              widget.email,
                                  (_) => Navigator.of(context).push(
                                      MaterialPageRoute(
                                      builder: (_) => const NewPasswordPage()
                                  )),
                                  (error) => showErrorDialog(context, error));
                        },
                        child: const Text("Сбросить пароль")),
                  ),
                  const SizedBox(height: 14,),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (_) => const SignUpPage()
                          )
                      );
                    },
                    child: RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(text: "Я вспомнил свой пароль! ",
                                  style: Theme.of(context)
                                      .textTheme.titleMedium),
                              TextSpan(text:  "Вернуться",
                                  style: Theme.of(context)
                                      .textTheme.titleMedium?.copyWith(
                                      color: colors.accent
                                  ))
                            ])),
                  ),
                  const SizedBox(height: 32)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
