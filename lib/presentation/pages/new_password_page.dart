import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessssion_1_2/common/colors.dart';
import 'package:sessssion_1_2/domain/new_password_presenter.dart';
import 'package:sessssion_1_2/presentation/pages/log_in_page.dart';
import 'package:sessssion_1_2/presentation/pages/sign_up_page.dart';
import 'package:sessssion_1_2/presentation/widgets/custom_text_field.dart';

import '../utils.dart';

class NewPasswordPage extends StatefulWidget{
  const NewPasswordPage({super.key});

  @override
  State<NewPasswordPage> createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {
  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  var presenter = NewPasswordPresenter();

  bool isObscurePassword = true;
  bool isObscureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83,),
            Text("Новый пароль",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите новый пароль",
              style: Theme.of(context).textTheme.titleMedium,),
            CustomTextField(
                label: "Пароль",
                hint: "**********",
                controller: password,
                enableObscure: isObscurePassword,),
            CustomTextField(
              label: "Подтвердить пароль",
              hint: "**********",
              controller: confirmPassword,
              enableObscure: isObscureConfirmPassword,),
            SizedBox(height: 18,),

            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressUpdatePassword(
                              password.text,
                                  (_) => Navigator.of(context)
                                      .pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (_) => LogInPage()
                                      ),
                                          (route) => false),
                                  (error) => showErrorDialog(context, error));
                          },
                        child: Text("Подтвердить")),
                  ),
                  SizedBox(height: 14,),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (_) => SignUpPage()
                          )
                      );
                    },
                    child: RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(text: "Я вспомнил свой пароль! ",
                                  style: Theme.of(context)
                                      .textTheme.titleMedium),
                              TextSpan(text:  "Вернуться",
                                  style: Theme.of(context)
                                      .textTheme.titleMedium?.copyWith(
                                      color: colors.accent
                                  ))
                            ])),
                  ),
                  SizedBox(height: 32)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
