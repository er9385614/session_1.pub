import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sessssion_1_2/common/colors.dart';
import 'package:sessssion_1_2/domain/holder_presenter.dart';

import '../utils.dart';

class HolderPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var presenter = HolderPresenter();
    var colors = LightColors();
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Center(
          child: SizedBox(
            width: double.infinity,
            height: 46,
            child: FilledButton(
                onPressed: (){
                 presenter.pressLogOut
                   ((p0) => exit(0)
                     , (error) => showErrorDialog(context, error));
                },
                child: Text("ВЫХОД")),
          ),
        ),
      ),
    );
  }
}