import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sessssion_1_2/common/colors.dart';
import 'package:sessssion_1_2/domain/forgot_password_presenter.dart';
import 'package:sessssion_1_2/domain/sign_up_presenter.dart';
import 'package:sessssion_1_2/presentation/pages/sign_up_page.dart';
import 'package:sessssion_1_2/presentation/pages/verify_otp_page.dart';
import 'package:sessssion_1_2/presentation/widgets/custom_text_field.dart';

import '../utils.dart';

class ForgotPasswordPage extends StatefulWidget{
  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  var email = TextEditingController();

  var presenter = ForgotPasswordPresenter();


  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83,),
            Text("Восстановление пароля",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите свою почту",
              style: Theme.of(context).textTheme.titleMedium,),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressResetPassword(
                              email.text,
                                  (_) => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (_) => VerifyOTPPage(
                                            email: email.text
                                          )
                                      )
                                  ), (error) => showErrorDialog(context, error)
                          );
                        },
                        child: Text("Отправить код")),
                  ),
                  SizedBox(height: 14,),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (_) => SignUpPage()
                          )
                      );
                    },
                    child: RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(text: "Я вспомнил свой пароль! ",
                                  style: Theme.of(context)
                                      .textTheme.titleMedium),
                              TextSpan(text:  "Вернуться",
                                  style: Theme.of(context)
                                      .textTheme.titleMedium?.copyWith(
                                      color: colors.accent
                                  ))
                            ])),
                  ),
                  SizedBox(height: 32)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
