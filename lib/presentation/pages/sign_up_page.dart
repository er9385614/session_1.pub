import 'package:flutter/material.dart';
import 'package:sessssion_1_2/common/colors.dart';
import 'package:sessssion_1_2/domain/sign_up_presenter.dart';
import 'package:sessssion_1_2/presentation/pages/holder_page.dart';
import 'package:sessssion_1_2/presentation/pages/log_in_page.dart';
import 'package:sessssion_1_2/presentation/widgets/custom_text_field.dart';

import '../utils.dart';

class SignUpPage extends StatefulWidget{
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var email = TextEditingController();
  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  var presenter = SignUpPresenter();

  bool isObscurePassword = true;
  bool isObscureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83,),
            Text("Создать аккаунт",
            style: Theme.of(context).textTheme.titleLarge,),
            const SizedBox(height: 8,),
            Text("Завершите регистрацию чтобы начать",
            style: Theme.of(context).textTheme.titleMedium,),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            CustomTextField(
                label: "Пароль",
                hint: "**********",
                controller: password,
                enableObscure: isObscurePassword,),
            CustomTextField(
                label: "Повторите пароль",
                hint: "**********",
                controller: confirmPassword,
                enableObscure: isObscureConfirmPassword,),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressSignUp(
                              email.text,
                              password.text,
                                  (_) => Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (_) => HolderPage()
                                      ),
                                          (route) => false),
                                  (error) => showErrorDialog(context, error));
                        },
                        child: const Text("Зарегистрироваться")),
                  ),
                  const SizedBox(height: 14,),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (_) => LogInPage()
                          )
                      );
                    },
                    child: RichText(
                        text: TextSpan(
                        children: [
                          TextSpan(text: "У меня уже есть аккаунт! ",
                              style: Theme.of(context)
                                  .textTheme.titleMedium),
                          TextSpan(text:  "Войти",
                              style: Theme.of(context)
                                  .textTheme.titleMedium?.copyWith(
                                color: colors.accent
                              ))
                        ])),
                  ),
                  const SizedBox(height: 32)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
