import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessssion_1_2/common/colors.dart';

class CustomTextField extends StatefulWidget{

  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;

  const CustomTextField(
      {
        super.key,
        required this.label,
        required this.hint,
        required this.controller,
        this.enableObscure = false});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  var colors = LightColors();
  bool isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 24,),
        Text(widget.label,
        style: Theme.of(context).textTheme.titleMedium,),
        SizedBox(height: 8,),
        SizedBox(
          width: double.infinity,
          height: 44,
          child: TextField(
            controller: widget.controller,
            obscuringCharacter: "*",
            obscureText: (widget.enableObscure) ? isObscure : false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleSmall,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: colors.subText,
              )),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4),
                  borderSide: BorderSide(
                  color: colors.subText,
                  )),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4),
                  borderSide: BorderSide(
                    color: colors.subText,
                  )),
              suffixIcon: (widget.enableObscure)
                  ? GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                child: Image.asset("assets/eye-slash.png"),
              ) : null
            ),
          ),
        )
      ],
    );
  }
}