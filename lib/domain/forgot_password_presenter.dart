import 'package:sessssion_1_2/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class ForgotPasswordPresenter{

  Future<void> pressResetPassword(
      String email,
      Function(void) onResponse,
      Function(String) onError) async {
    try {
      var result = await resetPassword(email);
      onResponse(result);
    } on AuthException catch (e) {
      onError(e.message);
    } on PostgrestException catch (e) {
      onError(e.message);
    } on Exception catch (e) {
      onError(e.toString());
    }
  }
}