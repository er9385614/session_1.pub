import 'package:sessssion_1_2/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class NewPasswordPresenter{

  Future<void> pressUpdatePassword(
      String newPassword,
      Function(UserResponse) onResponse,
      Function(String) onError
      ) async {
    try{
      var result = await updatePassword(newPassword);
      onResponse(result);
    }on AuthException catch (e) {
      onError(e.message);
    } on PostgrestException catch (e) {
      onError(e.message);
    } on Exception catch (e) {
      onError(e.toString());
    }
  }
}