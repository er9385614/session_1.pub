import 'package:sessssion_1_2/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class OTPPresenter{

  Future<void> pressOTP(
      String code,
      String email,
      Function(AuthResponse) onResponse,
      Function(String) onError
      ) async {
    try{
      dynamic result = await sendOTP(email, code);
      onResponse(result);
    }on AuthException catch (e) {
      onError(e.message);
    } on PostgrestException catch (e) {
      onError(e.message);
    } on Exception catch (e) {
      onError(e.toString());
    }
  }
}