import 'package:flutter/material.dart';
import 'package:sessssion_1_2/common/colors.dart';

var light = LightColors();

var lightTheme = ThemeData(
    textTheme: TextTheme(
      titleLarge: TextStyle(
          color: light.text,
          fontWeight: FontWeight.w500,
          fontFamily: "Roboto",
          fontSize: 24),
      titleMedium: TextStyle(
          color: light.subText,
          fontWeight: FontWeight.w500,
          fontFamily: "Roboto",
          fontSize: 14),
      titleSmall: TextStyle(
          color: light.hint,
          fontWeight: FontWeight.w500,
          fontFamily: "Roboto",
          fontSize: 14),
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15),
            textStyle: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 16,
                fontFamily: "Roboto",
                fontWeight: FontWeight.w500),
            backgroundColor: light.accent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ))),
    outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15),
            textStyle: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 16,
                fontFamily: "Roboto",
                fontWeight: FontWeight.w500),
            backgroundColor: light.accent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            )
        )
    )
);
