import 'package:flutter/material.dart';
import 'package:sessssion_1_2/common/theme.dart';
import 'package:sessssion_1_2/presentation/pages/sign_up_page.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  await Supabase.initialize(
      url: "https://czdvkuhvrfoyxllivtkw.supabase.co",
      anonKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImN6ZHZrdWh2cmZveXhsbGl2dGt3Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDkzOTYyNDcsImV4cCI6MjAyNDk3MjI0N30.D5ai7MeJKbAJkny_f-QCOC0GRE2r9G2moWlH9vsQMOA"
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: lightTheme,
      home: SignUpPage(),
    );
  }
}

